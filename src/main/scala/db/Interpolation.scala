package main.scala.db

import slick.driver.H2Driver.api._
import ListingLogic._

/**
  * Created by BatBox on 5/17/2016.
  */
abstract trait Interpolation {
  this: DB =>

  // makes it less messy to call close
  def close = db.close()

  // insure the listing depth is correct for the database
  def check(listing: Listing): Unit = {
    if (listing.length != depth)
      throw new IllegalArgumentException("bad listing length, cannot insert: " + listing.length)
  }

  // get id of listing, loops to create the sql command
  protected def getListingID(listing: Listing): DBIO[Seq[Int]] = {
    check(listing)
    var command = s"select list_id from listings where"
    for (i <- 0 until depth) {
      if (i != 0)
        command += s" and"
      command += s" word$i = '${listing(i)}'"
    }
    sql"#$command".as[Int]
  }


  //////////////////////////////////////////////////////////////////////////////////////////////
  ////for testing///////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  protected def testGetAllListings(table: String): DBIO[Seq[(Int, String, String, String)]] = sql"""select * from #${table}""".as[(Int, String, String, String)]

  protected def testGetAllCounts: DBIO[Seq[(Int, String, Int)]] = sql"""select * from counts""".as[(Int, String, Int)]

  protected def testGetNextWords : DBIO[Seq[(Int, Double, String)]] = sql"""select * from nextword""".as[(Int, Double, String)]
  //////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
}


// for building the database
trait Inserts extends Interpolation {
  this: DBBuilder =>

  protected def createDepthTable: DBIO[Int] = {
    sqlu"""create table depthtable(depth int)"""
  }

  protected def setDepth(depth: Int): DBIO[Int] = {
    sqlu"""insert into depthtable (depth) values (${depth})"""
  }


  protected def createListingTable: DBIO[Int] = {
    var command = "create table listings( list_id int primary key auto_increment"
    for (i <- 0 until depth)
      command += (", word" + i + " varchar not null")
    command += ")"
    sqlu"""#$command"""
  }

  protected def createCountsTable: DBIO[Int] = {
    sqlu"""
            create table counts(
            list_id int not null,
            word varchar,
            count int,
            foreign key (list_id) references listings(list_id),
            primary key (list_id, word)
            )
          """
  }

  protected def createNextWordTable: DBIO[Int] = {
    sqlu"""create table nextword(
             list_id int not null,
             cutoff double not null,
             word varchar not null,
             foreign key (list_id) references listings(list_id),
             primary key (list_id, word)
            )
          """
  }

  // add listing to table
  protected def insertListing(newListing: Listing): DBIO[Int] = {
    check(newListing)
    var command = "insert into listings ("
    for (i <- 0 until depth) {
      if (i != 0)
        command += ", "
      command += s"word${i}"
    }
    command += ") values ("
    for (i <- 0 until depth) {
      if (i != 0)
        command += ", "
      command += s"'${newListing(i)}'"
    }
    command += ")"
    sqlu"""#$command"""
  }

  protected def getAllListingIDs: DBIO[Seq[Int]] = sql"""select list_id from listings""".as[Int]

  // inserts a new listing/word or increments the count of a present listing/word
  protected def updateWordCount(listID: Int, word: String): DBIO[Int] =
    sqlu"""insert into counts (list_id, word, count) values (${listID}, ${word}, 1) on duplicate key update count = count + 1"""

  // get nextWord counts for a listing
  protected def getWordCounts(listID: Int): DBIO[Seq[(String, Int)]] =
    sql"""select word, count from counts where list_id = ${listID}""".as[(String, Int)]

  // insert a cutoff and the associated next word for a listing
  protected def insertNextWord(listID: Int, cutoff: Double, nextWord: String): DBIO[Int] =
    sqlu"""insert into nextword (list_id, cutoff, word) values (${listID}, ${cutoff}, ${nextWord})"""

  protected def dropTable(table: String): DBIO[Int] = sqlu"""drop table #${table}"""

}

// for building statements from the database
trait Reads extends Interpolation {
  this: DBReader =>

  protected def getDepth: DBIO[Seq[Int]] = {
    sql"""select * from depthtable""".as[Int]
  }

  protected def getNextWord(listID: Int, cutoff: Double): DBIO[Seq[String]] =
  sql"""select word from nextword where list_id = ${listID} and cutoff >= ${cutoff} order by cutoff""".as[String]

}
