package main.scala.db

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import slick.driver.H2Driver.api._
import ListingLogic._
import main.scala.parsing.{FacebookParser, SimpleReader}

/**
  * Created by BatBox on 5/13/2016.
  */


abstract class DB{

  val depth: Int
  val db: Database

}

// designed around how facebook formats the timeline download
class DBBuilder(val depth: Int) extends DB with Inserts with FacebookParser {
  val db = Database.forConfig("h2mem1")

  // handles the possibiliy of the database already being present
  try {
    Await.result(setup, Duration.Inf)
    println("setup complete")
  } catch {
    case e: Exception =>
      System.err.println("WARNING could not set up database, database is already present")
      sys.exit(1)
  }



  // create tables
  def setup: Future[Unit] = db.run(DBIO.seq(
    createDepthTable,
    createListingTable,
    createCountsTable,
    createNextWordTable,
    setDepth(depth)))

    // check if the listing exists, if not add it
  def addListing(newListing: Listing): Future[Int] = {
    db.run(
      getListingID(newListing)) flatMap { thing =>
      if (thing.isEmpty) db.run(insertListing(newListing))
      else Future {
        -1
      }
    }
  }

  // log nextWord
  def addToWordCount(listing: Listing, nextWord: String): Future[Int] = db.run(getListingID(listing)) flatMap { id => db.run(updateWordCount(id(0), nextWord)) }

  // adds an entry to nextword table
  def addToNextWord(listID: Int, cutoff: Double, nextWord: String): Future[Int] = db.run(insertNextWord(listID, cutoff, nextWord))

  // remove count table
  def dropCountTable: Future[Int] = db.run(dropTable("counts"))

  def populateNextWords: Future[Unit] = db.run(getAllListingIDs) map { ids =>
    // for each listing id
    for (id <- ids) {
      // get the words and counts associated with that id
      val wordCounts = Await.result(db.run(getWordCounts(id)), Duration.Inf)
      // get the total number of following words
      var total: Double = 0
      wordCounts map (total += _._2)
      // create a cutoff number and increment it for each word
      // each word has a window of {prior cutoff, its cutoff}, if
      // the random number generator's output is in that window,
      // it is the next word
      var currentCutoff: Double = 0
      wordCounts map (row => {
        currentCutoff += (row._2 / total)
        Await.result(db.run(insertNextWord(id, currentCutoff, row._1)), Duration.Inf)
      })
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////
  // for testing
  /////////////////////////////////////////////////////////////////////////////////////////
  def testGetListings = db.run(testGetAllListings("listings"))

  def testListingID(listing: Listing) = db.run(getListingID(listing))

  def testGetCounts = db.run(testGetAllCounts)

  def testNextWords: Future[Seq[(Int, Double, String)]] = db.run(testGetNextWords)

  /////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////

}

// DBReader should work with databases build using formats other than
// the facebook timeline download, but i haven't tested it as such
class DBReader extends DB with Reads with SimpleReader {
  val db = Database.forConfig("h2mem1")
  println("getting depth")
  val thing = Await.result(db.run(getDepth), Duration.Inf)
  println("depth table gives: " + thing)
  val depth = thing(0)

  // make the user aware if the listing is getting set multiple times
  try {
    Listing.setDepth(depth)
  } catch {
    case e: Exception => System.err.println("WARNING listing depth is set at " + Listing.getDepth + ", tried to reset to " + depth)
  }

  private def listingID(listing: Listing) = db.run(getListingID(listing))

  def returnNextWord(listing: Listing) = {
    val id = Await.result(listingID(listing), Duration.Inf)(0)
    val cutoff: Double = scala.util.Random.nextDouble()
    db.run(getNextWord(id, cutoff))
  }

  /////////////////////////////////////////////////////////////////////////////////////////
  // for testing
  /////////////////////////////////////////////////////////////////////////////////////////
  def testGetListings = db.run(testGetAllListings("listings"))

  def testListingID(listing: Listing) = db.run(getListingID(listing))

  def testGetCounts = db.run(testGetAllCounts)

  def testNextWords: Future[Seq[(Int, Double, String)]] = db.run(testGetNextWords)

  /////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////

}
