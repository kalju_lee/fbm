package main.scala.db

import slick.driver.H2Driver.api._
import slick.lifted.ProvenShape

/**
  * Created by BatBox on 5/14/2016.
  */

// table of nth words in a listing, with ids
class Listings(tag: Tag) extends Table[(Int, String, String, String, String, String, String, String)](tag, "listings") {

  def id: Rep[Int] = column[Int]("ID")

  def word1: Rep[String] = column[String]("WORD1")

  def word2: Rep[String] = column[String]("WORD2")

  def word3: Rep[String] = column[String]("WORD3")

  def word4: Rep[String] = column[String]("WORD4")

  def word5: Rep[String] = column[String]("WORD5")

  def word6: Rep[String] = column[String]("WORD6")

  def word7: Rep[String] = column[String]("WORD7")

  def * : ProvenShape[(Int, String, String, String, String, String, String, String)] =
    (id, word1, word2, word3, word4, word5, word6, word7)
}

// count of occurrences of words after listings, used for creating nextWord table
class Counts(tag: Tag) extends Table[(Int, String)](tag, "counts") {
  def id: Rep[Int] = column[Int]("ID")
  def word: Rep[String] = column[String]("WORD")
  def * : ProvenShape[(Int, String)] = (id, word)
}



// table of what word comes next, given a listing and number between 0 and 1
class NextWord(tag: Tag) extends Table[(Int, Double, String)](tag, "nextword") {
  def id: Rep[Int] = column[Int]("ID")

  def cutoff: Rep[Double] = column[Double]("CUTOFF")

  def nextWord: Rep[String] = column[String]("NEXTWORD")

  def * : ProvenShape[(Int, Double, String)] = (id, cutoff, nextWord)
}

