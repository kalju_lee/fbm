package main.scala.db.ListingLogic

/**
  * Created by BatBox on 5/19/2016.
  */

class Listing {

  // words is a var so it can be incremented by the function, private so it can't be set to
  // arbitrary values
  private var words: Vector[String] = (for (i <- 0 until Listing.depth) yield "").toVector

  // the apply method is the way to get words, i might add a way to get the entire vector, not
  // sure if that's necessary
  def apply(index: Int): String = words(index)

  // increments the listing by a word
  def increment(nextWord: String) {words = (words.tail) :+ nextWord}

  def length = words.length

  // when converted to a string, the components of a listing are comma-separated
  override def toString = {
    var result: String = ""
    for (i <- 0 until words.length) {
      if (i != 0)
        result += ", "
      result += words(i)}
    result
  }
}



object Listing {
  // all listings used should have the same depth
  // depth is a var so that there is some flexibility as to when it is set
  private var depth: Int = 0

  // will allow the depth to be set once.  I should probably have Listing throw
  // an exception if its methods are used when depth < 1
  def setDepth(newDepth: Int): Unit = {
    Listing.depth match {
      case 0 => if (newDepth > 0) Listing.depth = newDepth else throw new IllegalArgumentException("depth cannot be negative what would that even mean")
      case _ => throw new IllegalArgumentException("depth is already set - cannot set again")
    }
  }

  def getDepth = depth


}