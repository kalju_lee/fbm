package main.scala.parsing

import java.io.File
import main.scala.db.{DBBuilder, DBReader}
import scala.io.Source
import main.scala.db.ListingLogic.Listing
import scala.concurrent.duration.Duration
import scala.util.matching.Regex
import scala.concurrent.Await


/**
  * Created by BatBox on 5/20/2016.
  */
trait Parser {

  // deal with escape characters for quotes in sql
  def sqlify(word: String): String = {
    "'".r.replaceAllIn(word, "''")
  }

  def desqlify(word: String) = "''".r.replaceAllIn(word, "'")

}


trait FacebookParser extends Parser {
  this: DBBuilder =>

  // insure timeline file is present
  val file = new File("timeline.htm")
  if (!file.exists()) {
    System.err.println("cannot find 'timeline.htm'")
    sys.exit(-1)
  }
  val source = Source.fromFile(file, "UTF-8")


 // regexs
  val letter = new Regex("""[a-zA-Z]""", "c")
  val doubleQuotes = """&quot;""".r
  val singleQuotes = """&#039;""".r
  val div = """<div class="comment">|</div>""".r

  // prints information regarding setting depth
  try {
    Listing.setDepth(depth)
    println("tried to set listing depth to " + depth + ", is now " + Listing.getDepth)
  } catch {
    case e: Exception => System.err.println("WARNING: listing depth is set at " + Listing.getDepth + ", tried to reset to " + depth)
  }

  var listing: Listing = new Listing
  var queue: Vector[String] = Vector[String]()


  // split timeline file into individual posts, remove tags and metadata,
  // and deals with sql escape characters
  def parseLibrary = {
    source.mkString.split("</p><p>") map { entry =>
      """<div class="comment">.*</div>""".r.findFirstIn(entry)
    } map { comment =>
      comment match {
        case Some(thing) => parseEntry(singleQuotes.replaceAllIn(doubleQuotes.replaceAllIn(div.replaceAllIn(thing, ""), """""""), """'"""))
        case None =>
      }
    }
    source.close()
  }

  // create listings from a single post
  protected def parseEntry(entry: String) = {
    listing = new Listing
    entry foreach (thing => thing match {
      case letter(_*) => queue = queue :+ thing.toString
      case _ => {
        processQueue
        process(sqlify(thing.toString))
      }
    })
    processQueue
    process("")
  }

  def hasNext = source.hasNext

  // add word from the queue to the listing
  def processQueue = {
    if (!queue.isEmpty) {
      process(queue.foldLeft("")(_ + _))
      queue = Vector[String]()
    }
  }

  // adds whatever to the listing
  def process(thing: String) = {
     print("\r" + listing)
    Await.result(addListing(listing), Duration.Inf)
    val word = thing
    Await.result(addToWordCount(listing, word), Duration.Inf)
    listing.increment(word)
  }

}

// operations done on a pre-made database
trait SimpleReader extends Parser {
  this: DBReader =>

  // pulls words to create a new post
  def createText(length: Int = 0): String = {
    var output: String = ""
    var currentListing = new Listing

    if (length != 0) {
      for (i <- 0 until length) {
        println(currentListing)
        val nextWord: String = Await.result(returnNextWord(currentListing), Duration.Inf)(0)
        output += desqlify(nextWord)
        currentListing.increment(nextWord)
      }
    }
    else do {
      val nextWord: String = Await.result(returnNextWord(currentListing), Duration.Inf)(0)
      output += desqlify(nextWord)
      currentListing.increment(nextWord)
    } while (currentListing(depth - 1) != "")
    output
  }

}
