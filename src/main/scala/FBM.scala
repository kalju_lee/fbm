package main.scala

import db.{DBBuilder, DBReader}
import org.h2.jdbc.JdbcSQLException
import scala.concurrent.Await
import scala.concurrent.duration._


/**
  * Created by BatBox on 5/16/2016.
  */
object FBM extends App {

  val unknown = "mysterious arguments, type 'help' for help"
  val helpMessage = "help- brings you here\ndb {Int}- creates a database with a listing depth of {Int}\npost- creates a post"

  // dealing with two args
  if (args.length == 2)
    args(0) match {
      case "db" =>
        // if the second argument is valid, build a db
        try {
          val db = new DBBuilder(args(1).toInt)
          println("parsing input (don't panic if things look weird)...")
          db.parseLibrary
          println("\nparsing complete")
          println("generating nextword table...")
          Await.result(db.populateNextWords, Duration.Inf)
          println("nextword table complete")
          println("dropping count table...")
          Await.result(db.dropCountTable, Duration.Inf)
          println("count table dropped")
          println("creating database complete")
          db.close
        }
        // otherwise give an error and exit
        catch {
          case e: ArrayIndexOutOfBoundsException => println(unknown)
          case e: NumberFormatException => println(unknown)
          case e: JdbcSQLException => System.err.println("database already present")
        }
      // db is the only valid first argument if there is a second, anything else
      // causes an error and exit
      case _ => println(unknown)
    }

  // dealing with a single arg
  else if (args.length == 1)
    args(0) match {
      // prints a generated post
      case "post" =>
        try {
          println("creating reader")
          val reader = new DBReader
          println("reader created")
          println("post:")
          println(reader.createText(0))
          reader.close
        }
        // print error and exit if db can't be found
        catch {
          case e: JdbcSQLException => System.err.println("could not find depthtable, database may not have been created")
        }
      // prints help message
      case "help" => println(helpMessage)

      // any other arg prints and error and exits
      case _ => println(unknown)
    }
  // any other number of args prints error and exits
  else println(unknown)
}
