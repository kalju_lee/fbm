### The General Idea ###
This is a text generator using Markov chains, designed to work with the timeline.html file that Facebook generates for download.  
Basically it takes some source text (in this case Facebook posts) and determines the probability that any particular word will be followed by any other particular word.  It then uses these probabilities to "randomly" generate text that is similarish to the source texts.  
Unfortunately it doesn't include comments under a post, and it includes every post on your timeline, even ones you didn't write.


### How To Use It ###

**Short version** - Have timeline.htm (it will be in the html folder from Facebook) and FBM.jar in the same directory.  From that directory open the command prompt and type 
```
#!unix

java -jar FBM.jar
```
followed by 
```
#!python

db {some number, like 3.  but without the curly braces}
```
to make the database, or
```
#!python

post
```
 to generate a post from the database.  Using larger numbers will pretty quickly start just copying entire posts verbatim.  I find that somewhere in the 2-4 range works best, but YMMV.
### ###
 [Instructions on how to download your timeline are here.](https://www.facebook.com/help/131112897028467/)  
### ###
[You can download FBM.jar here.](https://bitbucket.org/kalju_lee/fbm/downloads)


###  ###
The jar file needs to be in the same directory as your timeline.htm file from the Facebook download.  There are only a couple of valid arguments: 
"help"- will print out a 'helpful' list of commands
"db {Int}"- creates a database with a depth of {Int}.  for example, "db 4" would create a database with listings that look back 4 "words".
"post"- Writes a post using the database.
If you want to update the database (like, if you have new Facebook posts you want included), you will need to delete the fbm.mv.db and fbm.trace.db files.  These files will be present if you attempt to make a post before creating a database.


### States and Such ###
For this project, "words" refer to continuous strings of letters, individual symbols and numbers, or whitespace characters.
Each state is a "listing", which is associated with a list of words and a probability that each word would be next.  Listings are given a depth, which is how many words they will contain.  This is effectively how many words back each state will look when determining the next word.  
Each new post restarts the listing as all empty strings, creating probabilities of what the first word could be.  The end of each post is used to determine the probability that any particular word will be the final one.  I'm pretty sure this will create posts that tend to be as long as the posts used to build the database, but I haven't proved it.


### Why I Did Things I Did ###
The number of possible listings grows exponentially the farther back the listings look.  The number of actual listings used is almost certainly going to be smaller then the max possible, but just in case I decided to use an H2 SQL database to store the listings, probabilities, and next words.  It was also an excuse to review how to SQL.

I used scala because I expected a bunch of pattern matching, and to refresh my understanding of the language.  The concise style is nice, though it often leaves me feeling I'm not writing code optimally (which is usually true).  

The Slick library used to SQL through scala involves more concurrency than I was ready to deal with.  Between sorting out scala and SQL bugs, I had no patience for the bizarre behavior the concurrent threads were causing; I did the ugly thing and just waited on any operation attempting to run asynchronously.  


### Things That I Did ###
Posts are separated out using regular expressions on the html tags.  I realize regex can be a problematic thing to use on html, but I figured auto-generated code from Facebook will be pretty standardized.  Once the posts are pulled out, they are gone over character-by-character.  If the character is a letter, it gets put in a queue.  If it is not a letter, then the contents of the queue is added to the listing as a word, the queue is reset, and the current character is listed as a possible "next word" for that listing.  The current character is then added to the listing and whatever comes next (be it a single character or the contents of the queue) is listed as a possible next word.  This continues to the end of the post, where the empty string is added as a possible next word.  When creating a post, the empty string as a "next word" signifies it's time to end the post.
I split the code into a few different sections: Interpolation for creating SQL commands, DB for database operations, Listing for the states in the Markov chain, Parser for pulling out Facebook posts and creating listings, and and FBM object for the main method.  
There are abstract Parser, DB, and Interpolation traits, with the idea being that it would make it easier later to create Markov chain text generators for sources in different formats.  Their subclasses are split into those dealing with creating a database, and those for reading the database and creating a post.  I'm not going to say I "regret" these decisions exactly, but I found that I probably should have planned this all out instead of making it up on the fly.


### Things I Will Do/ Should Have Done ###
Something I plan on doing in the future is adding an algorithm to insure opening/closing characters (", [, {, <, etc) are properly opened and closed.  I should really find a cleaner way to deal with concurrent threads.  I'd also like to create an easier ui, particularly around creating/deleting databases.  However, this project has already taken two weeks longer than I had planned on, and I am antsy to start my next thing.
Next time around I would (and will) start with a design document, instead of just thinking I should, half-assing two diagrams, and then diving right in.